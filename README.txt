Module: Privatemsg Mass Mailer
Author: Mirko Haaser <http://www.drupalist.de> mcgo@drupalist.de

Description
===========
Adds the posibility to send mass mail to selected roles or users. Sent mail is currently stored in your sent box. 

Requirements
============
* Privatemsg module http://drupal.org/project/privatemsg

Installation
============
* Just drop it into your module folder and enable it in the admin section.
* Set permission to send mass mail to selected users and or to all users in selectable roles

Usage
=====
* Point to your privatemsg - Inbox on /privatemsg/inbox. There are two new links when you have the corresponding permission. Klick on them, choose the recipient and you are done

