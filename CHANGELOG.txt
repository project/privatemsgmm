McGo - 12.3.2009
- Fixed issue #398558
- Added documentation to the code
- Validated the drupal coding standards with coder module (severity minor)

McGo - 13.1.2008
- authenticated user function doesn't work because the authenticated user have no role assignment with the role "authenticated user". Function deleted.
- Added support for Writing to Online User. A User is marked as online when the last access was not more than x minutes ago. The amount of minutes is stored in the "Who's online" Block.

McGo - 11.1.2008
- added hook_form_alter to present the access to massmailing on the inbox form of the privatemsg module (privatemsg/inbox) for the user with the propper rights.
- added support for sending massmail to users. You can chose the users directly on the form.
- changed the pathes from pn/massenmail to privatemsg/newbyrole | privatemsg/newbyuser

McGo - 10.1.2008
- The authenticated role can now be selected. On this was you can send privatemsg to all users that are currently online and even to user without a custom role assigned
